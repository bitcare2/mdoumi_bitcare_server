package kr.bit.bitcare.mdoumi;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 9.
 * @desc   : 결과 상수 정의
 */
public class ResultConstants {
	
	/**
	 * MDoumi Success Code
	 */
	public static final int MDOUMI_SERVICE_SUCCESS_CODE = 100;
			
	/**
	 * MDoumi Server Error Code
	 */
	public static final int MDOUMI_SERVICE_ERROR_CODE_SERVER = 101;
	
	/**
	 * MDoumi DB Error Code
	 */
	public static final int MDOUMI_SERVICE_ERROR_CODE_DB = 102;
	
	/**
	 * MDoumi Crypto Error Code
	 */
	public static final int MDOUMI_CRYPTO_ERROR_CODE = 103;
	
	/**
	 * MDoumi Crypto Error Message
	 */
	public static final String MDOUMI_CRYPTO_ERROR_MESSAGE = "파라미터 에러 입니다. 잠시후에 다시 시도해주세요.";
	
	
	/**
	 * MDoumi Server Error Message
	 */
	public static final String MDOUMI_SERVICE_ERROR_MESSAGE_SERVER = "서버에러 입니다. 잠시후에 다시 시도해주세요.";
	
	/**
	 * MDoumi DB Error Message 
	 */
	public static final String MDOUMI_SERVICE_ERROR_MESSAGE_DB = "DB에러 입니다. 잠시후에 다시 시도해주세요.";
}
