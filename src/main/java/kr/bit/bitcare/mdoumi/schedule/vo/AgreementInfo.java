package kr.bit.bitcare.mdoumi.schedule.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   : 동의서 정보 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AgreementInfo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1915470522592140874L;

	private String agreementName;
	
	private String agreementURl;
	
	private String dateTime;
	

}
