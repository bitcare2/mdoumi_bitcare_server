package kr.bit.bitcare.mdoumi.schedule.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   : 접수,진료,검사 등 대가 정보 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class WaitingInfo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8249762158283199995L;

	/**
	 * 진료과 위치
	 */
	private String departmentLocation;
	
	/**
	 * 진료과
	 */
	private String department;
	
	/**
	 * 의료진
	 */
	private String doctor;
	
	/**
	 * 총 대기번호 
	 */
	private String totalWaitingNumber;
	
	/**
	 * 대기번호 
	 */
	private String waitingNumber;
	
	/**
	 * 대기 내용 
	 */
	private String contents;
	
	/**
	 * 발급일시
	 */
	private String dateTime;
	
}
