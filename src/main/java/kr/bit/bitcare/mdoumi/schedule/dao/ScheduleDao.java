package kr.bit.bitcare.mdoumi.schedule.dao;

import java.util.ArrayList;

import org.springframework.stereotype.Repository;

import kr.bit.bitcare.mdoumi.common.AbstractDAO;
import kr.bit.bitcare.mdoumi.schedule.vo.NowScheduleInfoResultVO;
import kr.bit.bitcare.mdoumi.schedule.vo.ScheduleInfoParams;
import kr.bit.bitcare.mdoumi.schedule.vo.ToDayScheduleListResultVO;
import kr.bit.bitcare.mdoumi.schedule.vo.WaitingInfo;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   : 일정 DAO
 */
@Repository
public class ScheduleDao extends AbstractDAO{
	
	/**
	 * 현재일정 
	 * @param params
	 * @return
	 */
	public NowScheduleInfoResultVO nowScheduleInfo(ScheduleInfoParams params){
		
		NowScheduleInfoResultVO result = null;
		
		WaitingInfo waitingInfo = new WaitingInfo( "본관 1층 접수대", "원무과" ,"강창진" ,"351" ,"5" ,"" ,"201708101351" );
		
		result = new NowScheduleInfoResultVO("02", null, null, waitingInfo, null, null, null);
		
		return result;
	}
	
	/**
	 * 오늘일정
	 * @param params
	 * @return
	 */
	public ArrayList<ToDayScheduleListResultVO> todayScheduleList( ScheduleInfoParams params){
		
		ArrayList<ToDayScheduleListResultVO> result = new ArrayList<ToDayScheduleListResultVO>();
		
		result.add(new ToDayScheduleListResultVO("1", "02", "03", "홍길동님 내원해 주셔서 감사합니다. 번호표를 발급해주세요.", "0912", "", "", ""));
		result.add(new ToDayScheduleListResultVO("2", "03", "03", "접수 번호표가 발급되었습니다.", "0930", "15", "5", ""));
		result.add(new ToDayScheduleListResultVO("3", "08", "03", "홍길동님 1층 접수 창고로 방문해 주세요.", "0940", "", "", ""));
		result.add(new ToDayScheduleListResultVO("4", "04", "03", "본관 2층 비뇨기과로 반문해 주세요", "0955", "15", "1", ""));
		result.add(new ToDayScheduleListResultVO("5", "08", "03", "곧 진료 옝정입니다. 비뇨기과 앞에서 직원의 호명을 기다려 주세요.", "1005", "", "", ""));
		result.add(new ToDayScheduleListResultVO("6", "06", "02", "홍길동님 35,300(원)을 수납해주세요.", "1115", "", "", ""));			
		
		return result;
	}

	
	
		
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
