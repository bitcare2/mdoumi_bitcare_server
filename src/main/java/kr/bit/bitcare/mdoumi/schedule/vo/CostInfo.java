package kr.bit.bitcare.mdoumi.schedule.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   : 수납정보 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CostInfo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5045744248454119813L;

	private String totalCost;
	
	private String receiptURL;
	
	private String dateTime;
	
	
}
