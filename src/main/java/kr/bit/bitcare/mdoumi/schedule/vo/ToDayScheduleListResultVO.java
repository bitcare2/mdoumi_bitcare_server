package kr.bit.bitcare.mdoumi.schedule.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   : 오늘일정 리스트 결과
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ToDayScheduleListResultVO implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 332903838332602105L;

	/**
	 * 일정일련번호
	 */
	private String scheduleSerialID;
	
	/**
	 * 일장 타입 
	 * 00: 전체일정없음, 
	 * 01: 오늘일정없고 다음예약일있음, 
	 * 02: 오늘일정있고 번호표 발급 전상황
	 * 03 : 번호표 발급 후 접수 대기 상황
	 * 04 : 접수 완료후 진료 및 검사 대기 상황
	 * 05 : 원내외 처방전 발급상황
	 * 06 : 수납정보 발급상황
	 * 07 : 전자동의서 발급상황
	 * 08 : 안내문구
	 */
	private String scheduleType;
	
	/**
	 * 일정 상태 ( 01: 미완료, 02: 진행중, 03: 완료)
	 */
	private String status;
	
	/**
	 * 내용
	 */
	private String contents;
	
	/**
	 * 시간
	 */
	private String time;
	
	/**
	 * 총대기번호
	 */
	private String totalWaitingNumber;
	
	/**
	 * 대기번호
	 */
	private String waitingNumber;
	
	/**
	 * 연결URL
	 */
	private String contentsURL;

}
