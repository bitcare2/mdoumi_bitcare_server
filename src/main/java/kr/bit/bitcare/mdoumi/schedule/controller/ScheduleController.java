package kr.bit.bitcare.mdoumi.schedule.controller;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import kr.bit.bitcare.mdoumi.RequestMappingConstants;
import kr.bit.bitcare.mdoumi.ResultConstants;
import kr.bit.bitcare.mdoumi.common.APIControllerUtil;
import kr.bit.bitcare.mdoumi.schedule.dao.ScheduleDao;
import kr.bit.bitcare.mdoumi.schedule.vo.ScheduleInfoParams;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   : 일정 컨트롤러
 */
@RestController
@RequestMapping("/schedule")
public class ScheduleController {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private ScheduleDao scheduleDao;
	
	/**
	 * 현재일정 
	 * @param params
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_SCHEDULE_NOW_SCHEDULE_INFO}, produces="application/json")	
	public HashMap<String, Object> nowScheduleInfo(@RequestBody ScheduleInfoParams params){		
		logger.info("nowScheduleInfo( )");
			
		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", scheduleDao.nowScheduleInfo(params));
	}
	
	/**
	 * 오늘 일정 리스트를 얻는다. 
	 * @param params
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_SCHEDULE_TODAY_SCHEDULE_INFO}, produces="application/json")	
	public HashMap<String, Object> todayScheduleList(@RequestBody ScheduleInfoParams params){		
		logger.info("todayScheduleList( )");
			
		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", scheduleDao.todayScheduleList(params));
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
