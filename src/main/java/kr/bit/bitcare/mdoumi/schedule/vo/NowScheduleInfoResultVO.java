package kr.bit.bitcare.mdoumi.schedule.vo;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   : 현재일정 얻기 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class NowScheduleInfoResultVO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1932767301611355202L;

	/**
	 * 일정 타입 
	 * 00: 전체일정없음, 
	 * 01: 오늘일정없고 다음예약일있음, 
	 * 02: 오늘일정있고 번호표 발급 전상황
	 * 03 : 번호표 발급 후 접수 대기 상황
	 * 04 : 접수 완료후 진료 및 검사 대기 상황
	 * 05 : 원내외 처방전 발급상황
	 * 06 : 수납정보 발급상황
	 * 07 : 전자동의서 발급상황
	 * 08 : 안내문구	
	 */
	private String scheduleType;
		
	/**
	 * 다음예약 정보 리스트  
	 */
	private ArrayList<NextReservationList> nextReservationList;
	
	/**
	 * 오늘 예약 정보리스트
	 */
	private ArrayList<ToDayReservationList> toDayReservationList;
	
	/**
	 * 접수, 진료, 검사 등 대기정보 
	 */
	private WaitingInfo waitingInfo;
	
	/**
	 * 처방정보 
	 */
	private PrescriptionInfo prescriptionInfo;
	
	/**
	 * 수납정보 
	 */
	private CostInfo costInfo;
	
	/**
	 * 동의서 정보 
	 */
	private AgreementInfo agreementInfo;
	
	

}
