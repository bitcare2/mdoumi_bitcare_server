package kr.bit.bitcare.mdoumi.schedule.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   :
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ScheduleInfoParams implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3038015365795601421L;
	
	
	/**
	 * 환자 일련번호 
	 */
	private String patientSerialID;
	
	/**
	 * 환자 번호 
	 */
	private String patientNumber;
	
	/**
	 * 오늘날짜 
	 */
	private String date;
	
}
