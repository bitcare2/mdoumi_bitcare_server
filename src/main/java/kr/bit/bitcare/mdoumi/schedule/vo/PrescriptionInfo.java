package kr.bit.bitcare.mdoumi.schedule.vo;

import java.io.Serializable;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   : 처방전 정보 
 */
public class PrescriptionInfo implements Serializable{
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4756288713372028302L;

	/**
	 * 진료과 위치
	 */
	private String departmentLocation;
	
	/**
	 * 진료과
	 */
	private String department;
	
	/**
	 * 의료진
	 */
	private String doctor;
	
	/**
	 * 처방번호
	 */
	private String prescriptionNumber;
	
	/**
	 * 처방마코드URL
	 */
	private String prescriptionBarcode;
	
	/**
	 * 일시
	 */
	private String dateTime;
	
	
}
