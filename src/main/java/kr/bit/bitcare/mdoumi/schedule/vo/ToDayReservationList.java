package kr.bit.bitcare.mdoumi.schedule.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   : 오늘 예약 정보 리스트
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ToDayReservationList implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5177897831753057354L;

	/**
	 * 진료과
	 */
	private String department;
	
	/**
	 * 의료진
	 */
	private String doctor;
	
	/**
	 * 예약일시
	 */
	private String dateTime;
}
