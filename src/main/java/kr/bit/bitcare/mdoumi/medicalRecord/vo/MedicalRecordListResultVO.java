package kr.bit.bitcare.mdoumi.medicalRecord.vo;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   : 진료기록 내역 결과 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class MedicalRecordListResultVO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -829586501763622055L;

	private String dateTime;
	
	private String type;
	
	private String department;
	
	private String doctor;
	
	private String examinationName;
	
	private String prescriptionName;
	
	private ArrayList<PrescriptionList> prescriptionList;
	
	private String totalCost;
	
	private String reciptURL;
	
	
}
