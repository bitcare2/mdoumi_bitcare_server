package kr.bit.bitcare.mdoumi.medicalRecord.dao;

import java.util.ArrayList;

import org.springframework.stereotype.Repository;

import kr.bit.bitcare.mdoumi.common.AbstractDAO;
import kr.bit.bitcare.mdoumi.medicalRecord.vo.MedicalRecordListParams;
import kr.bit.bitcare.mdoumi.medicalRecord.vo.MedicalRecordListResultVO;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   : 진료기록 DAO
 */
@Repository
public class MedicalRecordDao extends AbstractDAO{

	/**
	 * 진료기록 
	 * @param params
	 * @return
	 */
	public ArrayList<MedicalRecordListResultVO> medicalRecordList( MedicalRecordListParams params){
		return null;
	}
}
