package kr.bit.bitcare.mdoumi.medicalRecord.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   : 진료내역 파라미터 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class MedicalRecordListParams implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5484356695645843266L;

	private String patientSerialID;
	
	private String patientNumber;
	
	private String type;
	
	private String startDate;
	
	private String endDate;
	
	
}
