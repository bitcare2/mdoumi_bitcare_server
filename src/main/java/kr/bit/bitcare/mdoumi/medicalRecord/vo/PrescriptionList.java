package kr.bit.bitcare.mdoumi.medicalRecord.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   : 처방내역 결과
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PrescriptionList implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7844754966372006956L;

	private String serialID;
	
	private String name;
	
	private String quantity;
	
	private String content;
	
	private String count;
	
	private String day;
	
	

}
