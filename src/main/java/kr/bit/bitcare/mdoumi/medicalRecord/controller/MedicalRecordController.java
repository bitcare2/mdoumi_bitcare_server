package kr.bit.bitcare.mdoumi.medicalRecord.controller;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import kr.bit.bitcare.mdoumi.RequestMappingConstants;
import kr.bit.bitcare.mdoumi.ResultConstants;
import kr.bit.bitcare.mdoumi.common.APIControllerUtil;
import kr.bit.bitcare.mdoumi.medicalRecord.dao.MedicalRecordDao;
import kr.bit.bitcare.mdoumi.medicalRecord.vo.MedicalRecordListParams;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   :
 */
@RestController
@RequestMapping("/medicalRecord")
public class MedicalRecordController {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private MedicalRecordDao medicalRecodrDao;
	
	/**
	 * 진료기록 내역 
	 * @param params
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_MEDICAL_RECORD_MEDICALRECORD_LIST}, produces="application/json")	
	public HashMap<String, Object> medicalRecordList(@RequestBody MedicalRecordListParams params){		
		logger.info("medicalRecordList( )");

		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", medicalRecodrDao.medicalRecordList(params));
	}
	
	
	
	
	
	
	
	
	
	
	
	
	

}
