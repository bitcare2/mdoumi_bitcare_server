package kr.bit.bitcare.mdoumi.health.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 11.
 * @desc   : 혈당 정보 삭제 파라미터
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class DeleteBloodSugarParams implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4848725471298396018L;

	private String patientSerialID;
	
	private String patientNumber;
	
	private String healthSerialID;
	
	
}
