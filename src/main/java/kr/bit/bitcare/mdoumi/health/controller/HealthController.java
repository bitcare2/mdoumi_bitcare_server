package kr.bit.bitcare.mdoumi.health.controller;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import kr.bit.bitcare.mdoumi.RequestMappingConstants;
import kr.bit.bitcare.mdoumi.ResultConstants;
import kr.bit.bitcare.mdoumi.common.APIControllerUtil;
import kr.bit.bitcare.mdoumi.health.dao.HealthDao;
import kr.bit.bitcare.mdoumi.health.vo.BloodPressureParams;
import kr.bit.bitcare.mdoumi.health.vo.BloodSugarParams;
import kr.bit.bitcare.mdoumi.health.vo.DeleteBloodPressureParams;
import kr.bit.bitcare.mdoumi.health.vo.DeleteBloodSugarParams;
import kr.bit.bitcare.mdoumi.health.vo.DeleteWeightParams;
import kr.bit.bitcare.mdoumi.health.vo.HealthListCountParam;
import kr.bit.bitcare.mdoumi.health.vo.HealthListParam;
import kr.bit.bitcare.mdoumi.health.vo.WeightParams;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   : 건강수첩 컨트롤러
 */
@RestController
@RequestMapping("/health")
public class HealthController {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private HealthDao healthDao;
	
	/**
	 * 건강수첩 내역 개수 
	 * @param params
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_HEALTH_HEALTH_LIST_COUNT}, produces="application/json")	
	public HashMap<String, Object> healthListCount(@RequestBody HealthListCountParam params){		
		logger.info("healthListCount( )");

		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", healthDao.healthListCount(params));
		
	}
	
	/**
	 * 건강수첩 내역 얻기 
	 * @param params
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_HEALTH_HEALTH_LIST}, produces="application/json")	
	public HashMap<String, Object> healthList(@RequestBody HealthListParam params){		
		logger.info("healthList( )");

		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", healthDao.healthList(params));
		
	}
	
	/**
	 * 혈압 등록 
	 * @param params
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_HEALTH_ADD_BLOOD_PRESSURE}, produces="application/json")
	public HashMap<String, Object> addBloodPressure (@RequestBody BloodPressureParams params){
		logger.info("addBloodPressure( )");
		
		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", healthDao.addBloodPressure(params));
	}
	
	/**
	 * 혈압 수정 
	 * @param params
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_HEALTH_MODIFY_BLOOD_PRESSURE}, produces="application/json")
	public HashMap<String, Object> modifyBloodPressure (@RequestBody BloodPressureParams params){
		logger.info("modifyBloodPressure( )");
		
		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", healthDao.modifyBloodPressure(params));
	}
	
	/**
	 * 혈압 삭제 
	 * @param params
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_HEALTH_DELETE_BLOOD_PRESSURE}, produces="application/json")
	public HashMap<String, Object> deleteBloodPressure(@RequestBody DeleteBloodPressureParams params){
		logger.info("modifyBloodPressure( )");
		
		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", healthDao.deleteBloodPressure(params));
	}
	
	/**
	 * 혈당 등록  
	 * @param params
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_HEALTH_ADD_BLOOD_SUGAR}, produces="application/json")
	public HashMap<String, Object> addBloodSugar(@RequestBody BloodSugarParams params){
		logger.info("addBloodSugar( )");
		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", healthDao.addBloodSugar(params));
	}
	
	/**
	 * 혈당 수정
	 * @param params
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_HEALTH_MODIFY_BLOOD_SUGAR}, produces="application/json")
	public HashMap<String, Object> modifyBloodSugar(@RequestBody BloodSugarParams params){
		logger.info("modifyBloodSugar( )");
		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", healthDao.modifyBloodSugar(params));
	}
	
	/**
	 * 혈당 삭제
	 * @param params
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_HEALTH_DELETE_BLOOD_SUGAR}, produces="application/json")
	public HashMap<String, Object> deleteBloodSugar(@RequestBody DeleteBloodSugarParams params){
		logger.info("deleteBloodSugar( )");
		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", healthDao.deleteBloodSugar(params));
	} 
	
	/**
	 * 체중등록 
	 * @param params
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_HEALTH_ADD_WEIGHT}, produces="application/json")
	public HashMap<String, Object> addWeight(@RequestBody WeightParams params){
		logger.info("addWeight( )");
		
		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", healthDao.addWeight(params));
	}
	
	/**
	 * 체중 수정 
	 * @param params
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_HEALTH_MODIFY_WEIGHT}, produces="application/json")
	public HashMap<String, Object> modifyWeight(@RequestBody WeightParams params){
		logger.info("modifyWeight( )");
		
		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", healthDao.modifyWeight(params));
	}

	/**
	 * 체중 삭제 
	 * @param params
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_HEALTH_DELETE_WEIGHT}, produces="application/json")
	public HashMap<String, Object> deleteWeight(@RequestBody DeleteWeightParams params){
		logger.info("deleteWeight( )");
		
		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", healthDao.deleteWeight(params));
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
