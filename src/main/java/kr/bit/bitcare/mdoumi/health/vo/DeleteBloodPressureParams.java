package kr.bit.bitcare.mdoumi.health.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 11.
 * @desc   : 혈압 삭제 파라미터
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class DeleteBloodPressureParams implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8531551474446508954L;

	private String patientSerialID;
	
	private String patientNumber;
	
	private String healthSerialID;
	
	
}
