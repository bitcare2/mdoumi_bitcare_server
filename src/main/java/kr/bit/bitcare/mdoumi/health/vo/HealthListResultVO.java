package kr.bit.bitcare.mdoumi.health.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   : 건강수첩 내역 결과
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class HealthListResultVO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8369986312848568074L;

	private String type;
	
	private String healthSerialID;
	
	private String systolicBP;
	
	private String diastolicBP;
	
	private String pulse;
	
	private String drugBP;
	
	private String exerciseTypeBP;
	
	private String dateTimeBP;
	
	private String bsValue;
	
	private String bsType;
	
	private String drugBS;
	
	private String exerciseTypeBS;
	
	private String dateTimeBS;
	
	private String weight;
	
	private String dateTimeWT;
	
	
}
