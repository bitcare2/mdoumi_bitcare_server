package kr.bit.bitcare.mdoumi.health.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 11.
 * @desc   :체증 등록 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class WeightParams implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2604667141160013088L;

	private String patientSerialID;
	
	private String patientNumber;
	
	private String healthSerialID;
	
	private String weight;
	
	private String dateTime;
	
}
