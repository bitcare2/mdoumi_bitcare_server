package kr.bit.bitcare.mdoumi.health.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 	
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 11.
 * @desc   : 혈압 등록/수정 파라미터 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class BloodPressureParams implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5219564604688212727L;

	private String healthSerialID;
	
	private String patientSerialID;
	
	private String patientNumber;
	
	private String systolicBP;
	
	private String diastolicBP;
	
	private String pulse;
	
	private String drugType;
	
	private String exerciseType;
	
	private String dateTime;
	
	
}
