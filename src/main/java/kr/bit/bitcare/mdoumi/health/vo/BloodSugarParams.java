package kr.bit.bitcare.mdoumi.health.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 11.
 * @desc   : 혈당 파라미터 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class BloodSugarParams implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1169138453756210944L;

	private String patientSerialID;
	
	private String patientNumber;
	
	private String healthSerialID;
	
	private String bloodSugar;
	
	private String bloodSugarType;
	
	private String drugType;
	
	private String exerciseType;
	
	private String dateTime;
}
