package kr.bit.bitcare.mdoumi.health.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   : 건강수첩 내역 파라미터 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class HealthListCountParam implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9119840879640335705L;

	/**
	 * 환자일련번호
	 */
	private String patientSerialID;
	
	/**
	 * 환자번호
	 */
	private String patientNumber;
	
	/**
	 * 타입 ( ALL : 전체, BP: 혈압, BS: 혈당, WT : 체중
	 */
	private String type;
	
}
