package kr.bit.bitcare.mdoumi.health.dao;

import java.util.ArrayList;

import org.springframework.stereotype.Repository;

import kr.bit.bitcare.mdoumi.common.AbstractDAO;
import kr.bit.bitcare.mdoumi.common.vo.CommonServiceVO;
import kr.bit.bitcare.mdoumi.health.vo.BloodPressureParams;
import kr.bit.bitcare.mdoumi.health.vo.BloodSugarParams;
import kr.bit.bitcare.mdoumi.health.vo.DeleteBloodPressureParams;
import kr.bit.bitcare.mdoumi.health.vo.DeleteBloodSugarParams;
import kr.bit.bitcare.mdoumi.health.vo.DeleteWeightParams;
import kr.bit.bitcare.mdoumi.health.vo.HealthListCountParam;
import kr.bit.bitcare.mdoumi.health.vo.HealthListParam;
import kr.bit.bitcare.mdoumi.health.vo.HealthListResultVO;
import kr.bit.bitcare.mdoumi.health.vo.WeightParams;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   :
 */
@Repository
public class HealthDao extends AbstractDAO{
	
	/**
	 * 건강수첩 리스트 갯수 
	 * @param params
	 * @return
	 */
	public String  healthListCount( HealthListCountParam params){
		return "100";
	}
	
	 
	/**
	 * 건강수첩 내역 
	 * @param params
	 * @return
	 */
	public ArrayList<HealthListResultVO> healthList(HealthListParam params){
		
		ArrayList<HealthListResultVO> result = new ArrayList<HealthListResultVO>();
		
		result.add(new HealthListResultVO("BP", "1", "120", "80", "65", "Y", "N", "20170810171312",     "", "", "", "", "",       "",""));
		result.add(new HealthListResultVO("BS", "2", "", "", "", "", "", "",     "100", "02", "N", "N", "20170811131212",       "",""));
		result.add(new HealthListResultVO("WT", "3", "", "", "", "", "", "",     "", "", "", "", "",       "75.5","20170813140012"));
		
		result.add(new HealthListResultVO("BP", "4", "120", "80", "65", "Y", "N", "20170814111312",     "", "", "", "", "",       "",""));
		result.add(new HealthListResultVO("BS", "5", "", "", "", "", "", "",     "100", "02", "N", "N", "20170815111212",       "",""));
		result.add(new HealthListResultVO("WT", "6", "", "", "", "", "", "",     "", "", "", "", "",       "75.5","20170815180012"));
		
		result.add(new HealthListResultVO("BP", "7", "120", "80", "65", "Y", "N", "20170817171312",     "", "", "", "", "",       "",""));
		result.add(new HealthListResultVO("BS", "8", "", "", "", "", "", "",     "100", "02", "N", "N", "20170817111212",       "",""));
		result.add(new HealthListResultVO("WT", "9", "", "", "", "", "", "",     "", "", "", "", "",       "75.5","20170817170012"));
		
		result.add(new HealthListResultVO("BP", "10", "120", "80", "65", "Y", "N", "20170819171312",     "", "", "", "", "",       "",""));
		result.add(new HealthListResultVO("BS", "11", "", "", "", "", "", "",     "100", "02", "N", "N", "20170819131212",       "",""));
		result.add(new HealthListResultVO("WT", "12", "", "", "", "", "", "",     "", "", "", "", "",       "75.5","20170819140012"));
		
		return result;
		
	}
	
	/**
	 * 혈압 등록 
	 * @param params
	 * @return
	 */
	public CommonServiceVO addBloodPressure ( BloodPressureParams params){
		return new CommonServiceVO("0","");
	}
	
	/**
	 * 혈압 수정
	 * @param params
	 * @return
	 */
	public CommonServiceVO modifyBloodPressure ( BloodPressureParams params){
		return new CommonServiceVO("0","");
	}
	
	/**
	 * 혈압 삭제  
	 * @param params
	 * @return
	 */
	public CommonServiceVO deleteBloodPressure(DeleteBloodPressureParams params){
		return new CommonServiceVO("0","");
	}
	
	/**
	 * 혈당 등록 
	 * @param params
	 * @return
	 */
	public CommonServiceVO addBloodSugar(BloodSugarParams params){
		return new CommonServiceVO("0","");
	}
	
	/**
	 * 혈당 수정
	 * @param params
	 * @return
	 */
	public CommonServiceVO modifyBloodSugar(BloodSugarParams params){
		return new CommonServiceVO("0","");
	}
	
	/**
	 * 혈당 삭제 
	 * @param params
	 * @return
	 */
	public CommonServiceVO deleteBloodSugar(DeleteBloodSugarParams params){
		return new CommonServiceVO("0","");
	}
	
	/**
	 * 체중 등록 
	 * @param params
	 * @return
	 */
	public CommonServiceVO addWeight( WeightParams params){
		return new CommonServiceVO("0","");
	}
	
	/**
	 * 체중 수정 
	 * @param params
	 * @return
	 */
	public CommonServiceVO modifyWeight( WeightParams params){
		return new CommonServiceVO("0","");
	}
	
	/**
	 * 체중 삭제  
	 * @param params
	 * @return
	 */
	public CommonServiceVO deleteWeight(DeleteWeightParams params){
		return new CommonServiceVO("0","");
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
