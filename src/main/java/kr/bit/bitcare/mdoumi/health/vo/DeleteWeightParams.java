package kr.bit.bitcare.mdoumi.health.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 11.
 * @desc   : 체중 삭제 파라미터
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class DeleteWeightParams implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9158966011864811278L;

	private String patientSerialID;
	
	private String patientNumber;
	
	private String healthSerialID;
	
	
}
