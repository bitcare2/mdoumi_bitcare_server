package kr.bit.bitcare.mdoumi.connection.controller;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import kr.bit.bitcare.mdoumi.RequestMappingConstants;
import kr.bit.bitcare.mdoumi.ResultConstants;
import kr.bit.bitcare.mdoumi.common.APIControllerUtil;
import kr.bit.bitcare.mdoumi.common.vo.CommonServiceVO;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 9.
 * @desc   : 접속여부 테스트 컨트롤러
 */
@RestController
@RequestMapping("/connection")
public class ConnectionController {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_CONNECTION}, produces="application/json")	
	public HashMap<String, Object> connection(){				
		logger.info("connection( )");
		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", new CommonServiceVO("0","1"));
	} 
}
 
