package kr.bit.bitcare.mdoumi.reservation.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   : 예약정보 리스트 결과
 */
 @Data
 @NoArgsConstructor
 @AllArgsConstructor
 @ToString
public class ReservationInfoResultVO  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6890221496682765832L;

	/**
	 * 예약일련번호
	 */
	private String reservationSerialID;
	
	/**
	 * 예약타입 ( 01: 진료완료, 02: 진료미완료)
	 */
	private String reservationType;
	
	/**
	 * 진료과 일련번호 
	 */
	private String departmentSerialID;
	
	/**
	 * 진료과
	 */
	private String department;
	
	/**
	 * 의료진 일련번호 
	 */
	private String doctorSerialID;
	
	/**
	 * 의료진 
	 */
	private String doctor;
	
	/**
	 * 예약일자
	 */
	private String date;
	
	/**
	 * 예약시간 
	 */
	private String time;
	
	/**
	 * 진료의 타입 ( 01: 일반, 02: 선택진료의)
	 */
	private String doctorType;
	
	/**
	 * 약관동의 (Y/N)
	 */
	private String acceptTerms;
	
	/**
	 * 예약메모내용
	 */
	private String memo;
		

}
