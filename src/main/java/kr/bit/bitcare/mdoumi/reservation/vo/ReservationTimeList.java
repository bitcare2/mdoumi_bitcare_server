package kr.bit.bitcare.mdoumi.reservation.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   : 예약가능시간 정보 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ReservationTimeList implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2798719701253450313L;
	
	/**
	 * 예약시간
	 */
	private String time;
	
	/**
	 * 예약가능시간 타입 (Y/N)
	 */
	private String timeType;
	
	
}
