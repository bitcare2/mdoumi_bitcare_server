package kr.bit.bitcare.mdoumi.reservation.dao;

import java.util.ArrayList;

import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;

import kr.bit.bitcare.mdoumi.common.AbstractDAO;
import kr.bit.bitcare.mdoumi.common.vo.CommonParams;
import kr.bit.bitcare.mdoumi.common.vo.CommonServiceVO;
import kr.bit.bitcare.mdoumi.reservation.vo.AddReservationInfoParams;
import kr.bit.bitcare.mdoumi.reservation.vo.DeleteReservationParams;
import kr.bit.bitcare.mdoumi.reservation.vo.DepartmentListResultVO;
import kr.bit.bitcare.mdoumi.reservation.vo.DoctorListResultVO;
import kr.bit.bitcare.mdoumi.reservation.vo.ReservationInfoResultVO;
import kr.bit.bitcare.mdoumi.reservation.vo.ReservationListParams;
import kr.bit.bitcare.mdoumi.reservation.vo.ReservationTimeInfoParams;
import kr.bit.bitcare.mdoumi.reservation.vo.ReservationTimeInfoResultVO;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   :
 */
@Repository
public class ReservationDAO extends AbstractDAO {
	
	/**
	 * 예약정보 얻기
	 * @param params
	 * @return
	 */
	public ArrayList<ReservationInfoResultVO> reservaationList(ReservationListParams params){		
		
		ArrayList<ReservationInfoResultVO> result = new ArrayList<ReservationInfoResultVO>();
		
		result.add(new ReservationInfoResultVO("1", "01", "12345", "비뇨기과", "54321", "강창진", "20170802", "1300", "01", "N", "" ));
		
		result.add(new ReservationInfoResultVO("2", "01", "12345", "비뇨기과", "54321", "강창진", "20170808", "1200", "02", "Y", "예약부탁드립니다." ));
		
		result.add(new ReservationInfoResultVO("3", "01", "12341", "정형외과", "54323", "고승희", "20170810", "1430", "01", "N", "발목이 너무 아퍼요." ));
		
		result.add(new ReservationInfoResultVO("4", "02", "12341", "정형외과", "54323", "고승희", "20170817", "0930", "02", "Y", "아직도 너무 마퍼요." ));
		
		return result;		
	}
	
	
	/**
	 * 진료과 얻기 
	 * @param params
	 * @return
	 */
	public ArrayList<DepartmentListResultVO> departmentInfo(CommonParams params){
		
		ArrayList<DepartmentListResultVO> result = new ArrayList<DepartmentListResultVO>();
		
		result.add(new DepartmentListResultVO("1","정형외과"));
		result.add(new DepartmentListResultVO("2","소아과"));
		result.add(new DepartmentListResultVO("3","피부과"));
		result.add(new DepartmentListResultVO("4","치과"));
		result.add(new DepartmentListResultVO("5","내분비내과"));
		result.add(new DepartmentListResultVO("6","순환기내과"));
		result.add(new DepartmentListResultVO("7","심장외과"));
		result.add(new DepartmentListResultVO("8","가정의학과"));
		result.add(new DepartmentListResultVO("9","신경외과"));
		result.add(new DepartmentListResultVO("10","성형외과"));		
		return result;		
	}
	
	/**
	 * 의료진 정보 
	 * @param params
	 * @return
	 */
	public ArrayList<DoctorListResultVO> doctorInfo(CommonParams params){
		
		ArrayList<DoctorListResultVO> result = new ArrayList<DoctorListResultVO>();
		
		result.add(new DoctorListResultVO("1", "정형외과", "1", "홍닥터", "", "척추 추체 압박골절, 두부 및 축추 외상","오전진료 : 월,수 \\ 오후진료 : 화,목 \\ 토요일 : 2,4 번 째주 ","1990 비트대학교 의과대학 졸업 \\ 2000 비트대학교 의과대학교 박사취득",""));
		result.add(new DoctorListResultVO("1","정형외과", "2", "강창진", "", "요추디스크, 경추디스크, 척추관협착증, 척추 전방전위증, 척추 추제압박골정 등","오전진료 : 월,목 \\ 오후진료 : 화,수 \\ 토요일 : 1,3 번 째주 ","1991 비트대학교 의과대학 졸업 \\ 2002 비트대학교 의과대학교 박사취득",""));		
		result.add(new DoctorListResultVO("1","정형외과", "3", "김닥터", "", "척추 추체 압박골절, 두부 및 축추 외상","오전진료 : 월,수 \\ 오후진료 : 화,목 \\ 토요일 : 2,4 번 째주 ","1990 비트대학교 의과대학 졸업 \\ 2000 비트대학교 의과대학교 박사취득",""));
		result.add(new DoctorListResultVO("1","정형외과", "4", "추자모", "", "요추디스크, 경추디스크, 척추관협착증, 척추 전방전위증, 척추 추제압박골정 등","오전진료 : 월,목 \\ 오후진료 : 화,수 \\ 토요일 : 1,3 번 째주 ","1991 비트대학교 의과대학 졸업 \\ 2002 비트대학교 의과대학교 박사취득",""));		
		result.add(new DoctorListResultVO("1","정형외과", "5", "김진수", "", "척추 추체 압박골절, 두부 및 축추 외상","오전진료 : 월,수 \\ 오후진료 : 화,목 \\ 토요일 : 2,4 번 째주 ","1990 비트대학교 의과대학 졸업 \\ 2000 비트대학교 의과대학교 박사취득",""));
		result.add(new DoctorListResultVO("1","정형외과", "6", "박찬혁", "", "요추디스크, 경추디스크, 척추관협착증, 척추 전방전위증, 척추 추제압박골정 등","오전진료 : 월,목 \\ 오후진료 : 화,수 \\ 토요일 : 1,3 번 째주 ","1991 비트대학교 의과대학 졸업 \\ 2002 비트대학교 의과대학교 박사취득",""));
		
		
		return result;
		
	}
	
	/**
	 * 최근 진료받은 의료진 정보
	 * @param params
	 * @return
	 */
	public DoctorListResultVO latelyDepartmentDoctorInfo(CommonParams params){
		
		DoctorListResultVO result = new DoctorListResultVO("1", "정형외과", "1", "홍닥터", "", "척추 추체 압박골절, 두부 및 축추 외상","오전진료 : 월,수 \\ 오후진료 : 화,목 \\ 토요일 : 2,4 번 째주 ","1990 비트대학교 의과대학 졸업 \\ 2000 비트대학교 의과대학교 박사취득","");
		
		return result;
		
	}
	
	/**
	 * 의료진의 예약가능 정보 얻기  - 향후 수정 
	 * @param params
	 * @return
	 */
	public ReservationTimeInfoResultVO  reservationTimeInfo(ReservationTimeInfoParams params){
		
		
		
		return new ReservationTimeInfoResultVO("","",null);
	}
	
	/**
	 * 예약 저장 
	 * @param params
	 * @return
	 */
	public CommonServiceVO addReservationInfoParams(AddReservationInfoParams params){
		
		return new CommonServiceVO("0","");
	}
	
	/**
	 * 예약수정
	 * @param params
	 * @return
	 */
	public CommonServiceVO modifyReservationInfoParams(ReservationInfoResultVO params){
		return new CommonServiceVO("0","");		
	}
	
	/**
	 * 예약삭제
	 * @param params
	 * @return
	 */
	public CommonServiceVO deleteReservationInfoParams(DeleteReservationParams params){
		return new CommonServiceVO("0","");
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
