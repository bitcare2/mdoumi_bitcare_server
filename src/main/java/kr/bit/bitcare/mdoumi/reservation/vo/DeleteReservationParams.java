package kr.bit.bitcare.mdoumi.reservation.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   : 예약삭제 파라미터
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class DeleteReservationParams implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6099541577709365173L;

	private String patientSerialID;
	
	private String patientNumber;
	
	private String reservationSerialID;
	
	
}
