package kr.bit.bitcare.mdoumi.reservation.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   : 진료과 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class DepartmentListResultVO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5550232366570071067L;

	/**
	 * 진료과 일련번호 
	 */
	private String departmentSerialID;
	
	/**
	 * 진료과 명 
	 */
	private String departmentName;
	

}
