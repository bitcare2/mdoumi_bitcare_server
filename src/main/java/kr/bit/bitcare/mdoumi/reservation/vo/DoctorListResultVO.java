package kr.bit.bitcare.mdoumi.reservation.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   : 의료진 리스트 결과 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class DoctorListResultVO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4082294152729109159L;
	
	/**
	 * 진료과 일련번호
	 */
	private String departmentSerialID;
	/**
	 * 진료과명
	 */
	private String departmentName;
	
	/**
	 * 의료진 일련번호
	 */
	private String doctorSerialID;
	
	/**
	 * 의료진명
	 */
	private String doctorName;

	/**
	 * 의료진 이미지URL
	 */
	private String doctorImg;
	
	/**
	 * 전공분야
	 */
	private String majorField;
	
	/**
	 * 진료일정
	 */
	private String schedule;
	
	/**
	 * 학력정보
	 */
	private String achivement;
	
	/**
	 * 경력정보
	 */
	private String career;
	
	
	
}
