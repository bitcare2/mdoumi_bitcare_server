package kr.bit.bitcare.mdoumi.reservation.controller;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import kr.bit.bitcare.mdoumi.RequestMappingConstants;
import kr.bit.bitcare.mdoumi.ResultConstants;
import kr.bit.bitcare.mdoumi.common.APIControllerUtil;
import kr.bit.bitcare.mdoumi.common.vo.CommonParams;
import kr.bit.bitcare.mdoumi.reservation.dao.ReservationDAO;
import kr.bit.bitcare.mdoumi.reservation.vo.AddReservationInfoParams;
import kr.bit.bitcare.mdoumi.reservation.vo.DeleteReservationParams;
import kr.bit.bitcare.mdoumi.reservation.vo.ReservationInfoResultVO;
import kr.bit.bitcare.mdoumi.reservation.vo.ReservationListParams;
import kr.bit.bitcare.mdoumi.reservation.vo.ReservationTimeInfoParams;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   : 예약 컨트롤러
 */
@RestController
@RequestMapping("/reservation")
public class ReservationController {
	
private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private ReservationDAO reservationDao;
	
	/**
	 * 예약정보 
	 * @param params
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_RESERVATION_RESERVATION_LIST}, produces="application/json")	
	public HashMap<String, Object> reservaationList(@RequestBody ReservationListParams params){		
		logger.info("reservaationList( )");

		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", reservationDao.reservaationList(params));
	}
	
	
	
	/**
	 * 진료과 정보 얻기 
	 * @param params
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_RESERVATION_DEPARTMENT_INFO}, produces="application/json")	
	public HashMap<String, Object> departmentInfo(@RequestBody CommonParams params){		
		logger.info("departmentInfo( )");

		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", reservationDao.departmentInfo(params));

	}

	/**
	 * 의료진 정보 얻기 
	 * @param params
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_RESERVATION_DOCTOR_INFO}, produces="application/json")	
	public HashMap<String, Object> doctorInfo(@RequestBody CommonParams params){		
		logger.info("doctorInfo( )");

		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", reservationDao.doctorInfo(params));

	}
	
	/**
	 *최근 진료받은 의요진 정보 얻기 
	 * @param params
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_RESERVATION_LATELY_DEPARTMENT_DOCTOR_INFO}, produces="application/json")	
	public HashMap<String, Object> latelyDepartmentDoctorInfo(@RequestBody CommonParams params){		
		logger.info("latelyDepartmentDoctorInfo( )");

		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", reservationDao.latelyDepartmentDoctorInfo(params));

	}

	
	
	
	/**
	 * 의료진 예약가능 정보 얻기
	 * @param params
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_RESERVATION_RESERVATION_TIME_INFO}, produces="application/json")	
	public HashMap<String, Object> reservationTimeInfo(@RequestBody ReservationTimeInfoParams params){		
		logger.info("reservationTimeInfo( )");

		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", reservationDao.reservationTimeInfo(params));

	}
	
	


	/**
	 * 예약정보 저장
	 * @param params
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_RESERVATION_ADD_RESERVATION_INFO}, produces="application/json")	
	public HashMap<String, Object> addReservationInfoParams(@RequestBody AddReservationInfoParams params){		
		logger.info("addReservationInfoParams( )");

		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", reservationDao.addReservationInfoParams(params));

	}

	/**
	 * 예약정보 수정
	 * @param params
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_RESERVATION_MODIFY_RESERVATION_INFO}, produces="application/json")
	public HashMap<String, Object> modifyReservationInfoParams(@RequestBody ReservationInfoResultVO params){		
		logger.info("modifyReservationInfoParams( )");

		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", reservationDao.modifyReservationInfoParams(params));
	}
	

	
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_RESERVATION_DELETE_RESERVATION_INFO}, produces="application/json")
	public HashMap<String, Object> deleteReservationInfoParams(@RequestBody DeleteReservationParams params){		
		logger.info("modifyReservationInfoParams( )");

		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", reservationDao.deleteReservationInfoParams(params));
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
