package kr.bit.bitcare.mdoumi.reservation.vo;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   : 의료진 예약 가능 정보 결과 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ReservationTimeInfoResultVO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1611867217419757453L;

	/**
	 * 일자
	 */
	private String date;
	
	/**
	 * 예약가능일자 타입 
	 */
	private String dateType;
	
	/**
	 * 예약가능시간 리스트 정보 
	 */
	private ArrayList<ReservationTimeList> reservationTime;
	
}
