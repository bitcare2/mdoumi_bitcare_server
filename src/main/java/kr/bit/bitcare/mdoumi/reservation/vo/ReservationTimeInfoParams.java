package kr.bit.bitcare.mdoumi.reservation.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   : 의료진의 예약가능 정보 파라미터
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ReservationTimeInfoParams implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1748649807341476403L;

	private String patientSerialID;
	
	private String patientNumber;
	
	private String doctorSerialID;
	
	private String startDate;
	
	private String endDate;
}
