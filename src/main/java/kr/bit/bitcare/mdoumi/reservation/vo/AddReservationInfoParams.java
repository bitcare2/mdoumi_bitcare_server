package kr.bit.bitcare.mdoumi.reservation.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   : 예약정보 저장 파라미터
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AddReservationInfoParams implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5399245333761239206L;

	private String patientSerialID;
	
	private String patientNumber;
	
	private String departmentSerialID;
	
	private String department;
	
	private String doctorSerialID;
	
	private String doctor;
	
	private String date;
	
	private String time;
	
	private String doctorType;
	
	private String acceptTerms;
	
	private String memo;
	
	

}
