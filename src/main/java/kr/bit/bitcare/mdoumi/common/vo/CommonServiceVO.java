package kr.bit.bitcare.mdoumi.common.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 서비스 공통 결과 
 * @author WooMyeongGwan
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommonServiceVO {
	
	/**
	 * 결과코드 
	 */
	private String resultCode;
	
	/**
	 * 결과메시지 
	 */
	private String resultMsg;
	
	
}
