package kr.bit.bitcare.mdoumi.common.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   : 공통 파라미터
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CommonParams implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2446697220290425553L;
	
	/**
	 * 환자 일련번호
	 */
	private String patientSerialID;
	
	/**
	 * 환자 번호 
	 */
	private String patientNumber;
	

}
