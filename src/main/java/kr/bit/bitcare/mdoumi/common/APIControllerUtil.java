package kr.bit.bitcare.mdoumi.common;

import java.util.HashMap;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 9.
 * @desc   : controller util
 */
public class APIControllerUtil {
	
	
	
	public static HashMap<String,Object> ResultTOMap(int serviceCode, String serviceMsg, Object obj){
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("serviceCode", serviceCode);
		map.put("serviceMsg", serviceMsg);
		map.put("result", obj);
		
		System.out.println(map.toString());
		
		return map;
	}

}
