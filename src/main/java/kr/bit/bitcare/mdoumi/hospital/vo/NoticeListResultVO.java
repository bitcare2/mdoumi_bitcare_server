package kr.bit.bitcare.mdoumi.hospital.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 11.
 * @desc   : 공지사항 결과 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class NoticeListResultVO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5155731782947423272L;

	private String noticeserialID;
	
	private String contents;
	
	private String dateTime;
	
}
