package kr.bit.bitcare.mdoumi.hospital.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 11.
 * @desc   : 원내전화 결과 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class DepartmentPhoneResultVO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3765196760972234048L;

	private String departmentName;
	
	private String departmentPhoneNumber;
	
	
}
