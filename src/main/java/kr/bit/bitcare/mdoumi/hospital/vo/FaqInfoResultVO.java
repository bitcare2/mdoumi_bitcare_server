package kr.bit.bitcare.mdoumi.hospital.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 11.
 * @desc   : 자주하는 질문 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class FaqInfoResultVO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1316199764357140243L;

	private String faqSerialID;
	
	private String title;
	
	private String contents;
	
	private String dateTime;
	
	

}
