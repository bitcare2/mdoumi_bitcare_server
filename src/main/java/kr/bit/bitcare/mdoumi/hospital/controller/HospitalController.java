package kr.bit.bitcare.mdoumi.hospital.controller;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import kr.bit.bitcare.mdoumi.RequestMappingConstants;
import kr.bit.bitcare.mdoumi.ResultConstants;
import kr.bit.bitcare.mdoumi.common.APIControllerUtil;
import kr.bit.bitcare.mdoumi.hospital.dao.HospitalDao;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 11.
 * @desc   : 병원안내 컨트롤러
 */
@RestController
@RequestMapping("/hospital")
public class HospitalController {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private HospitalDao hospitalDao;
	
	/**
	 * 병원정보 병원소개
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_HOSPITAL_HOSPITAL_INFO}, produces="application/json")	
	public HashMap<String, Object> hospitalImg(){		
		logger.info("hospitalImg( )");

		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", hospitalDao.hospitalInfo());
		
	}

	/**
	 * 전체 의료진 정보 얻기 
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_HOSPITAL_ALL_DOCTOR_INFO}, produces="application/json")	
	public HashMap<String, Object> allDoctorInfo(){		
		logger.info("allDoctorInfo( )");

		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", hospitalDao.allDoctorInfo());
		
	}
	

	/**
	 * 원내전화 얻기
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_HOSPITAL_DEPARTMENT_PHONE_INFO}, produces="application/json")	
	public HashMap<String, Object> departmentPhoneInfo(){		
		logger.info("departmentPhoneInfo( )");

		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", hospitalDao.departmentPhoneInfo());
		
	}
	
	
	/**
	 * 병원정보 오시는길 
	 * @param params
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_HOSPITAL_STREET_INFO}, produces="application/json")	
	public HashMap<String, Object> streetInfo(){		
		logger.info("streetInfo( )");

		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", hospitalDao.streetInfo());
		
	}
	 
	/**
	 * 공지사항
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_HOSPITAL_NOTICE_LIST}, produces="application/json")	
	public HashMap<String, Object> noticeList(){		
		logger.info("noticeList( )");

		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", hospitalDao.noticeList());
		
	}

	/**
	 * 자주하는질문 
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_HOSPITAL_FAQ_INFO}, produces="application/json")	
	public HashMap<String, Object> faqInfo(){		
		logger.info("faqInfo( )");

		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", hospitalDao.faqInfo());
		
	}	
	
	
	
	

}
