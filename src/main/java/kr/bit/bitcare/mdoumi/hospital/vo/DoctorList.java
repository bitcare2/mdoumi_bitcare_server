package kr.bit.bitcare.mdoumi.hospital.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 11.
 * @desc   : 의료진 정보 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class DoctorList implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1339066027457582965L;

	private String departmentName;
	
	private String doctorSerialID;
	
	private String doctorName;
	
	private String doctorImg;
	
	private String majorField;
	
	private String schedule;
	
	private String achievement;
	
	private String career;
	
	
	

}
