package kr.bit.bitcare.mdoumi.hospital.dao;

import java.util.ArrayList;

import org.springframework.stereotype.Repository;

import kr.bit.bitcare.mdoumi.common.AbstractDAO;
import kr.bit.bitcare.mdoumi.hospital.vo.AllDoctorInfoResultVO;
import kr.bit.bitcare.mdoumi.hospital.vo.DepartmentPhoneResultVO;
import kr.bit.bitcare.mdoumi.hospital.vo.FaqInfoResultVO;
import kr.bit.bitcare.mdoumi.hospital.vo.HospitalInfoResultVO;
import kr.bit.bitcare.mdoumi.hospital.vo.NoticeListResultVO;
import kr.bit.bitcare.mdoumi.hospital.vo.StreetInfoResultVO;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 11.
 * @desc   : 병원안내 DAO
 */
@Repository
public class HospitalDao extends AbstractDAO{
	
	/**
	 * 병원소개
	 * @return
	 */
	public HospitalInfoResultVO hospitalInfo(){
		
		HospitalInfoResultVO result = new HospitalInfoResultVO(
				"http://122.199.225.68/img/bit.PNG",
				"비트컴퓨터는 '소프트웨어'라는 단어조차 생소하던 83년, 국내대학생 벤처 1호, 소프트웨어 전문 회사 1호로 설립된 회사입니다. 대학 3학년에 재학중이던 조현정 회장이 450만원의 자본금, 직원 2명으로, 호텔 객실에서 창업하여 97년 KOSDAQ상장을 계기로 본격적인 성장기에 진입한, 내실과 미래가치가 높게 평가되고 있는 리딩 벤처기업입니다",
				"비트컴퓨터는 의료정보 분야의 전문성을 바탕으로 성장성이 높은 분야인 U-health 시장의 선점과 해외 시장의 본격 공략을 통해 헬스케어 전문기업으로 입지를 확고히 하겠다는 목표를 세우고 있습니다. 원격진료시스템 공급 1위업체로 U-health 시장의 대표기업으로 자리매김하고 있으며, 해외사업에 있어서도 태국, 일본, 우크라이나, 카자흐스탄, 미국, 몽골 등에 진출해 상승세를 이어나가고 있습니다. ");
		
		return result;
	}
	
	/**
	 * 전체 의료진 정보 
	 * @return
	 */
	public ArrayList<AllDoctorInfoResultVO> allDoctorInfo(){
		
		return null;
	}
	
	/**
	 * 원내전화 번호 얻기 
	 * @return
	 */
	public ArrayList<DepartmentPhoneResultVO> departmentPhoneInfo(){
		
		ArrayList<DepartmentPhoneResultVO> result = new ArrayList<DepartmentPhoneResultVO>();
		
		result.add(new DepartmentPhoneResultVO("대표전화", "02-3486-1234"));
		result.add(new DepartmentPhoneResultVO("진료예약", "02-3486-1212"));
		result.add(new DepartmentPhoneResultVO("예약확인", "02-3486-3849"));
		result.add(new DepartmentPhoneResultVO("분편접수", "02-3486-1278"));
		result.add(new DepartmentPhoneResultVO("원무과", "02-3486-1111"));
		
		return result;
	}
	
	
	/**
	 * 병원오시는길 
	 * @return
	 */
	public StreetInfoResultVO streetInfo(){
		
		StreetInfoResultVO result = new StreetInfoResultVO(
				"http://122.199.225.68/img/hospital.PNG",
				"지하철 2호선 강남역 하차후 7번출구 , 신분당선 강남역 하차 후 5번출구 ",
				"네비게이션 비트병원 검색",
				"내원환자 무료 주차 2시간, 면회객 무료 주차 2시간, 이후 30분당 3000원 , *주차장이 매우 협소하므로 면회객분들은 대중교통 이용을 부탁드리겠습니다. "  );
		
		return result;
	}
	
	/**
	 * 공지사항
	 * @return
	 */
	public ArrayList<NoticeListResultVO> noticeList(){
		
		ArrayList<NoticeListResultVO> result = new ArrayList<NoticeListResultVO>();
		
		result.add(new NoticeListResultVO("1", " 비트병원 개원 35주년 기념 특별 기획...  ", "20170811142412"));
		result.add(new NoticeListResultVO("2", " 모바일 병원 서비스 'M-Doumi' 오픈을 안내 드립니다.  앱스토어에 'M-Doumi' 로 검색하시면다운받으실수 있습니다. ", "20170812142412"));
		
		return result;
	}
	
	/**
	 * 자주하는질문 
	 * @return
	 */
	public ArrayList<FaqInfoResultVO> faqInfo(){
		ArrayList<FaqInfoResultVO>  result = new ArrayList<FaqInfoResultVO> ();
		
		result.add(new FaqInfoResultVO("1", "갑자기 앱 접속이 되지 않습니다.", "다른 인터넷 사이트 접속이 되는지 우선 확인 부탁드립니다.....", "20170811143112"));
		result.add(new FaqInfoResultVO("2", "로그아웃은 어떻게 하나요?", "M-Doumi 앱 내에 설정--> 로그아웃 버튼으로 가능하십니다.", "20170811143112"));
		result.add(new FaqInfoResultVO("2", "회원정보는 어떻게 변경하나요?", "앱 설정버튼을 누르신 후 회원정보 변경 메뉴로 들어가시면병경하실수 있습니다.", "20170811143112"));
		
		return result;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
