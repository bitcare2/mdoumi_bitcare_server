package kr.bit.bitcare.mdoumi.hospital.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 11.
 * @desc   : 병원소개 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HospitalInfoResultVO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5398964630446460461L;

	private String hospitalImg;
	
	private String introduce;
	
	private String ceoMessage;
}
