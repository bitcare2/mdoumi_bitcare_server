package kr.bit.bitcare.mdoumi.hospital.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 11.
 * @desc   : 병원오시는길 결과
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class StreetInfoResultVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 896391662528798131L;
	
	private String mapImg;
	
	private String publicTransport;
	
	private String ownCar;
	
	private String parking;
	

}
