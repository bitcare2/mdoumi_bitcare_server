package kr.bit.bitcare.mdoumi.hospital.vo;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 11.
 * @desc   : 병원정보 의료진 전체 결과 
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AllDoctorInfoResultVO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4267860975764657807L;

	private String departmentList;
	
	private String departmentSerialID;
	
	private String departmentName;
	
	private ArrayList<DoctorList> doctorList;
	
	
}
