package kr.bit.bitcare.mdoumi;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 9.
 * @desc   : 호출 URL 상수정의
 */
public class RequestMappingConstants {
		
	/**
	 * 접속 여부 
	 */
	public static final String REQUEST_MAPPING_URL_CONNECTION = "/connection.do";
	
	/**
	 * 로그인 
	 */
	public static final String REQUEST_MAPPING_URL_PATIENT_LOGIN = "/login.do";
	
	/**
	 * 아이디 찾기
	 */
	public static final String REQUEST_MAPPING_URL_PATIENT_SEARCH_ID = "/searchID.do";
	
	/**
	 * 패스워드 찾기
	 */
	public static final String REQUEST_MAPPING_URL_PATIENT_SEARCH_PW = "/searchPW.do";
	
	/**
	 * 패스워드 수정
	 */
	public static final String REQUEST_MAPPING_URL_PATIENT_MODIFY_PW = "/modifyPW.do";
	
	/**
	 * 아이디 중복확인
	 */
	public static final String REQUEST_MAPPING_URL_PATIENT_ID_CONFIRM = "/idConfirm.do";
	
	/**
	 * 등록확인
	 */
	public static final String REQUEST_MAPPING_URL_PATIENT_JOIN_CONFIRM = "/joinConfirm.do";
	
	/**
	 * 환자정보 등록 
	 */
	public static final String REQUEST_MAPPING_URL_PATIENT_ADD_PATIENT_INFO = "/addPatientInfo.do";
	
	/**
	 * 환자정보 수정
	 */
	public static final String REQUEST_MAPPING_URL_PATIENT_MODIFY_PATIENT_INFO = "/modifyPatientInfo.do";
	
	/**
	 * 현재일정 얻기 (메인)
	 */
	public static final String REQUEST_MAPPING_URL_SCHEDULE_NOW_SCHEDULE_INFO = "/nowScheduleInfo.do";
	
	/**
	 * 오늘일정 얻기
	 */
	public static final String REQUEST_MAPPING_URL_SCHEDULE_TODAY_SCHEDULE_INFO = "/todayScheduleList.do";
	
	/**
	 * 예약정보 리스트
	 */
	public static final String REQUEST_MAPPING_URL_RESERVATION_RESERVATION_LIST = "/reservationList.do";
	
	/**
	 * 진료과 정보 얻기 
	 */
	public static final String REQUEST_MAPPING_URL_RESERVATION_DEPARTMENT_INFO = "/departmentInfo.do";
	
	/**
	 * 의료진 정보 얻기 
	 */
	public static final String REQUEST_MAPPING_URL_RESERVATION_DOCTOR_INFO = "/doctorInfo.do";
	
	/**
	 * 최근 진료받은 의료진 정보 
	 */
	public static final String REQUEST_MAPPING_URL_RESERVATION_LATELY_DEPARTMENT_DOCTOR_INFO = "/latelyDepartmentDoctorInfo.do";
	
	/**
	 * 의료진 예약가능 정보 얻기 
	 */
	public static final String REQUEST_MAPPING_URL_RESERVATION_RESERVATION_TIME_INFO = "/reservationTimeInfo.do";
	
	/**
	 * 예약정보 저장 
	 */
	public static final String REQUEST_MAPPING_URL_RESERVATION_ADD_RESERVATION_INFO = "/addReservationInfo.do";
	/**
	 * 예약정보 수정
	 */
	public static final String REQUEST_MAPPING_URL_RESERVATION_MODIFY_RESERVATION_INFO = "/modifyReservationInfo.do";
	
	/**
	 * 예약삭제
	 */
	public static final String REQUEST_MAPPING_URL_RESERVATION_DELETE_RESERVATION_INFO = "/deleteReservationInfo.do";
	

	/**
	 * 진료기록 내역 
	 */
	public static final String REQUEST_MAPPING_URL_MEDICAL_RECORD_MEDICALRECORD_LIST = "/medicalRecordList.do";
	
	/**
	 * 건강수첩 리스트 갯수
	 */
	public static final String REQUEST_MAPPING_URL_HEALTH_HEALTH_LIST_COUNT = "/healthListCount.do";
	
	/**
	 * 건강수첩 내역 
	 */
	public static final String REQUEST_MAPPING_URL_HEALTH_HEALTH_LIST = "/healthList.do";
	
	/**
	 * 혈압 등록 
	 */
	public static final String REQUEST_MAPPING_URL_HEALTH_ADD_BLOOD_PRESSURE = "/addBloodPressure.do";
	
	/**
	 * 혈압 수정
	 */
	public static final String REQUEST_MAPPING_URL_HEALTH_MODIFY_BLOOD_PRESSURE = "/modifyBloodPressure.do";
	
	
	/**
	 * 혈압 삭제
	 */
	public static final String REQUEST_MAPPING_URL_HEALTH_DELETE_BLOOD_PRESSURE = "/deleteBloodPressure.do";
	
	/**
	 * 혈당 등록 
	 */
	public static final String REQUEST_MAPPING_URL_HEALTH_ADD_BLOOD_SUGAR = "/addBloodSugar.do";
	
	/**
	 * 혈당 수정
	 */
	public static final String REQUEST_MAPPING_URL_HEALTH_MODIFY_BLOOD_SUGAR = "/modifyBloodSugar.do";
	
	
	/**
	 * 혈당 삭제
	 */
	public static final String REQUEST_MAPPING_URL_HEALTH_DELETE_BLOOD_SUGAR = "/deleteBloodSugar.do";
	
	
	
	/**
	 * 체중 등록 
	 */
	public static final String REQUEST_MAPPING_URL_HEALTH_ADD_WEIGHT = "/addWeight.do";
	
	/**
	 * 체중 수정
	 */
	public static final String REQUEST_MAPPING_URL_HEALTH_MODIFY_WEIGHT = "/modifyWeight.do";
	
	
	/**
	 * 체중 삭제
	 */
	public static final String REQUEST_MAPPING_URL_HEALTH_DELETE_WEIGHT = "/deleteWeight.do";
	
	
	/**
	 * 병원소개
	 */
	public static final String REQUEST_MAPPING_URL_HOSPITAL_HOSPITAL_INFO = "/hospitalInfo.do";
	
	/**
	 * 의료진 소개
	 */
	public static final String REQUEST_MAPPING_URL_HOSPITAL_ALL_DOCTOR_INFO = "/allDoctorInfo.do";
	
	/**
	 * 원내전화 
	 */
	public static final String REQUEST_MAPPING_URL_HOSPITAL_DEPARTMENT_PHONE_INFO = "/departmentPhoneInfo.do";
	
	/**
	 * 오시는길
	 */
	public static final String REQUEST_MAPPING_URL_HOSPITAL_STREET_INFO= "/streetInfo.do";
	
	/**
	 * 공지사항
	 */
	public static final String REQUEST_MAPPING_URL_HOSPITAL_NOTICE_LIST= "/noticeList.do";
	
	/**
	 * 자주하는질문
	 */
	public static final String REQUEST_MAPPING_URL_HOSPITAL_FAQ_INFO= "/faqInfo.do";
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
