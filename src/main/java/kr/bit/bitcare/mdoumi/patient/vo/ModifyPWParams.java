package kr.bit.bitcare.mdoumi.patient.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   : 패스워드 수정 파라미터
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ModifyPWParams implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3420494089792064203L;

	/**
	 * 휴대폰 SID
	 */
	private String deviceSID;
	
	/**
	 * 휴대폰 레지스트ID
	 */
	private String deviceRegID;
	
	/**
	 * os타입 ( IOS : 아이폰, AND 안드로이드)
	 */
	private String deviceType;
	
	/**
	 * 환자 일련번호 
	 */
	private String patientSerialID;
	
	/**
	 * 환자 패스워드
	 */
	private String password;
	
	
}
