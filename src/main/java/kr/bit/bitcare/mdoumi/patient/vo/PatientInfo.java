package kr.bit.bitcare.mdoumi.patient.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   : 환자정보 
 */
@Data 
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PatientInfo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2037296000675178516L;

	
	/**
	 * 환자일련번호
	 */
	private String patientSerialID;
	
	/**
	 * 환자 아이디
	 */
	private String id;
	
	/**
	 * 환자 패스워드
	 */
	private String password;
	
	/**
	 * 환자 이름 
	 */
	private String name;
	
	/**
	 * 환자 성별 
	 */
	private String gender;
	
	/**
	 * 생년월일 
	 */
	private String birthDay;
	
	/**
	 * 휴대폰 번호 
	 */
	private String phoneNumber;
	
	/**
	 * 이메일 
	 */
	private String email;
	
	/**
	 * 환자번호 
	 */
	private String patientNumber;
	
	/**
	 * 휴대폰 SID
	 */
	private String deviceSID;
	
	/**
	 * 휴대폰 레지스트ID
	 */
	private String deviceRegID;
	
	/**
	 * OS타입 ( IOS : 아이폰, AND : 안드로이드)
	 */
	private String deviceType;
	
	/**
	 * 복약알림
	 */
	private String drugYN;
	
	/**
	 * 예약알림
	 */
	private String reservationYN;
	
	/**
	 * 병원 일련번호
	 */
	private String hospitalSerialID;
	
	/**
	 * 환자 ID카드 URL
	 */
	private String patientIDCard;
	
	
	
	
}
