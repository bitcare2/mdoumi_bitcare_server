package kr.bit.bitcare.mdoumi.patient.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class LoginResultVO  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1160222091312595202L;

	/**
	 * 결과코드 (0 : 성공, 1: 실패) 
	 */
	private String resultCode;
	
	/**
	 * 로그인 메시지  ex) 입력하신 정보가 맞는 회원이 없습니다.
	 * 						병원에 등록된 정보인지 화인해주세요.
	 */
	private String resultMsg;
	
	/**
	 * 환자 타입 ( 01: 환자번호 등록환자, 02: 환자번호 미등록환자)
	 */
	private String patientType;
	
	/**
	 * 환자 시스템 일련번호 
	 */
	private String patientSerialID;
	
	/**
	 * 환자 번호 
	 */
	private String patientNumber;

	/**
	 * 환자 진료카드 
	 */
	private String patientCareCard;
	
	/**
	 * 패스워드 타입 ( Y/ N) 
	 */
	private String pwType;
	
	
	
	

}
