package kr.bit.bitcare.mdoumi.patient.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   : 휴대폰 찾기 파라미터 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SearchPWParams implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1057946940656923404L;

	/**
	 * 휴대폰SID
	 */
	private String deviceSID;
	
	/**
	 * 휴대폰 레지스트ID
	 */
	private String deviceRegID;
	
	/**
	 * OS타입 ( IOS : 아이폰, AND : 안드로이드)
	 */
	private String deviceType;
	
	/**
	 * 환자 이름 
	 */
	private String name;
	
	/**
	 * 환자 아이디
	 */
	private String id;
	
	/**
	 * 휴대폰 번호 
	 */
	private String phoneNumber;
	

}
