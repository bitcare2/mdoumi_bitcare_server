package kr.bit.bitcare.mdoumi.patient.dao;

import org.springframework.stereotype.Repository;

import kr.bit.bitcare.mdoumi.common.AbstractDAO;
import kr.bit.bitcare.mdoumi.common.vo.CommonServiceVO;
import kr.bit.bitcare.mdoumi.patient.vo.AddPatientInfoResultVO;
import kr.bit.bitcare.mdoumi.patient.vo.IDConfirmParams;
import kr.bit.bitcare.mdoumi.patient.vo.JoinConfirmParams;
import kr.bit.bitcare.mdoumi.patient.vo.LoginParams;
import kr.bit.bitcare.mdoumi.patient.vo.LoginResultVO;
import kr.bit.bitcare.mdoumi.patient.vo.ModifyPWParams;
import kr.bit.bitcare.mdoumi.patient.vo.PatientInfo;
import kr.bit.bitcare.mdoumi.patient.vo.SearchIDParams;
import kr.bit.bitcare.mdoumi.patient.vo.SearchPWParams;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 9.
 * @desc   : 환자 관련 DAO
 */
@Repository
public class PatientDao extends AbstractDAO{
	
	private final static String SQL_NAME_SPACE = "patient.";

	
	/**
	 * 로그인 
	 * @param params
	 * @return
	 */
	public LoginResultVO login(LoginParams params){		
		
		LoginResultVO result = new LoginResultVO("0", "", "01", "100001", "126791","http://122.199.225.68/barcode/barcode.png","N");	
		
		return result ;		
	}
	
	/**
	 * 아이디 찾기
	 * @param params
	 * @return
	 */
	public CommonServiceVO searchID(SearchIDParams params){
		
		return new CommonServiceVO("0","hong12***");
	}
	/**
	 * 패스워드 찾기
	 * @param params
	 * @return
	 */
	public CommonServiceVO searchPW(SearchPWParams params){
		
		return new CommonServiceVO("0","");
	}
	
	/**
	 * 패스워드 수정
	 * @param params
	 * @return
	 */
	public CommonServiceVO modifyPW(ModifyPWParams params){
		
		return new CommonServiceVO("0","");
	}
	
	/**
	 * 아이디 중복 확인
	 * @param params
	 * @return
	 */
	public CommonServiceVO idConfirm(IDConfirmParams params){
		return new CommonServiceVO("0","");
	}
	
	/**
	 *  환자 등록확인 
	 * @param params
	 * @return
	 */
	public CommonServiceVO joinConfirm( JoinConfirmParams params){
		return new CommonServiceVO("0","");
	}
	
	/**
	 * 환자 정보 등록 
	 * @param params
	 * @return
	 */
	public AddPatientInfoResultVO addPatientInfo(PatientInfo params){
		
		return new AddPatientInfoResultVO("0","", params);
	}
	
	/**
	 * 환자정보 수정
	 * @param params
	 * @return
	 */
	public CommonServiceVO modifyPatientInfo(PatientInfo params){		
		return new CommonServiceVO("0","");
	}
	

}
