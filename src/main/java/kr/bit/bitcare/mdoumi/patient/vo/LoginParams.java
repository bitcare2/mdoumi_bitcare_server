package kr.bit.bitcare.mdoumi.patient.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 9.
 * @desc   : 로그인 파라미터 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class LoginParams implements Serializable{
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5914207836332084718L;
	

	/**
	 * 휴대폰SID
	 */
	private String deviceSID;
	
	/**
	 * 휴대폰 레지스트ID 
	 */
	private String deviceRegID;
	
	/**
	 * OS타입 ( IOS : 아이폰, AND : 안드로이드)
	 */
	private String deviceType;
	
	/**
	 * 환자 아이디  
	 */
	private String id;
	
	/**
	 * 환자 패스워드 
	 */
	private String password;
	

}
