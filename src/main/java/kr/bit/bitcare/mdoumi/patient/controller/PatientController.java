package kr.bit.bitcare.mdoumi.patient.controller;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import kr.bit.bitcare.mdoumi.RequestMappingConstants;
import kr.bit.bitcare.mdoumi.ResultConstants;
import kr.bit.bitcare.mdoumi.common.APIControllerUtil;
import kr.bit.bitcare.mdoumi.patient.dao.PatientDao;
import kr.bit.bitcare.mdoumi.patient.vo.IDConfirmParams;
import kr.bit.bitcare.mdoumi.patient.vo.JoinConfirmParams;
import kr.bit.bitcare.mdoumi.patient.vo.LoginParams;
import kr.bit.bitcare.mdoumi.patient.vo.ModifyPWParams;
import kr.bit.bitcare.mdoumi.patient.vo.PatientInfo;
import kr.bit.bitcare.mdoumi.patient.vo.SearchIDParams;
import kr.bit.bitcare.mdoumi.patient.vo.SearchPWParams;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 9.
 * @desc   : 환자 관련 컨트롤러
 */
@RestController
@RequestMapping("/patient")
public class PatientController {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private PatientDao patientDao;
	
	/**
	 * 환자 로그인 
	 * @param params
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_PATIENT_LOGIN}, produces="application/json")	
	public HashMap<String, Object> login(@RequestBody LoginParams params){		
		logger.info("login( )");
			
		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", patientDao.login(params));
	} 
	
	/**
	 * 아이디 찾기
	 * @param params
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_PATIENT_SEARCH_ID}, produces="application/json")
	public HashMap<String, Object>searchID(@RequestBody SearchIDParams params){
		logger.info("searchID( )");
		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", patientDao.searchID(params));
	}
	
	/**
	 * 패스워드 찾기
	 * @param params
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_PATIENT_SEARCH_PW}, produces="application/json")
	public HashMap<String, Object>searchPW(@RequestBody SearchPWParams params){
		logger.info("searchPW( )");
		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", patientDao.searchPW(params));
	}
	
	/**
	 * 패스워드 수정
	 * @param params
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_PATIENT_MODIFY_PW}, produces="application/json")
	public HashMap<String, Object>modifyPW(@RequestBody ModifyPWParams params){
		logger.info("modifyPW( )");
		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", patientDao.modifyPW(params));
	}
	
	/**
	 * 아이디 중복확인 
	 * @param params
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_PATIENT_ID_CONFIRM}, produces="application/json")
	public HashMap<String, Object>idConfirm(@RequestBody IDConfirmParams params){
		logger.info("idConfirm( )");
		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", patientDao.idConfirm(params));
	}
	
	/**
	 * 등록확인
	 * @param params
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_PATIENT_JOIN_CONFIRM}, produces="application/json")
	public HashMap<String, Object>joinConfirm(@RequestBody JoinConfirmParams params){
		logger.info("joinConfirm( )");
		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", patientDao.joinConfirm(params));
	}
	
	/**
	 * 환자 정보 등록 
	 * @param params
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_PATIENT_ADD_PATIENT_INFO}, produces="application/json")
	public HashMap<String, Object>addPatientInfo(@RequestBody PatientInfo params){
		logger.info("addPatientInfo( )");		
		
		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", patientDao.addPatientInfo(params));
	}
	
	/**
	 * 환자정보 수정
	 * @param params
	 * @return
	 */
	@RequestMapping( method={RequestMethod.POST}, value={RequestMappingConstants.REQUEST_MAPPING_URL_PATIENT_MODIFY_PATIENT_INFO}, produces="application/json")	
	public HashMap<String, Object>modifyPatientInfo(@RequestBody PatientInfo params){
		logger.info("modifyPatientInfo( )");	
		
		return APIControllerUtil.ResultTOMap(ResultConstants.MDOUMI_SERVICE_SUCCESS_CODE, "", patientDao.modifyPatientInfo(params));
	}
	
	

}
