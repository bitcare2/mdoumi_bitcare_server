package kr.bit.bitcare.mdoumi.patient.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   : 아이디 중복 확인 파라미터 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class IDConfirmParams implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6003179509959877647L;

	private String deviceSID;
	
	private String deviceRegID;
	
	private String deviceType;
	
	private String id;
	
	
}
