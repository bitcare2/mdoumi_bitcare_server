package kr.bit.bitcare.mdoumi.patient.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   : 환자 등록 결과 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AddPatientInfoResultVO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6462768830306274511L;

	private String resultCode;
	
	private String resultMsg;
	
	private PatientInfo patientInfo;
	

}
