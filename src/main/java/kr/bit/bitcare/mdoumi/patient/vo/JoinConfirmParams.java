package kr.bit.bitcare.mdoumi.patient.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author : WooMyeongGwan
 * @date   : 2017. 8. 10.
 * @desc   : 등록확인 파라미터 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class JoinConfirmParams implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2635566350639001453L;

	/**
	 * 환자이름 
	 */
	private String name;
	
	/**
	 * 생년월일 
	 */
	private String birthDay;
	
	/**
	 * 성별 ( M : 남 / F : du)
	 */
	private String gender;
	
	/**
	 * 휴대폰번호 
	 */
	private String phoneNumber;
	
	/**
	 * 휴대폰SID
	 */
	private String deviceSID;
	
	/**
	 * 휴대폰 레지스트 ID
	 */
	private String deviceRegID;
		
	/**
	 * OS 타입 ( IOS : 아이폰, AND : 안드로이드) 
	 */
	private String deviceType;
	
	
}
